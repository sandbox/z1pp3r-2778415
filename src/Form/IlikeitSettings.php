<?php

namespace Drupal\ilikeit\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * IlikeitSettings class extending FormBase.
 */
class IlikeitSettings extends FormBase {

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'ilikeit_settings';
  }

  /**
   * Form constructor.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The form structure.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['water_level'] = array(
      '#type' => 'fieldset',
      '#title' => $this->t('Water Level API'),
      '#collapsed' => TRUE,
      '#collapsible' => TRUE,
    );

    $form['water_level']['water_levels_url'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Water Levels REST Web Service URL'),
      '#description' => t('This will be used to get the water level data for display.'),
      '#default_value' => \Drupal::state()->get('water_levels_url'),
      '#required' => TRUE,
    );

    $form['emergency_broadcast'] = array(
      '#type' => 'fieldset',
      '#title' => $this->t('Emergency Broadcast'),
      '#collapsed' => FALSE,
      '#collapsible' => FALSE,
    );

    $form['emergency_broadcast']['emergency_broadcast_message'] = array(
      '#type' => 'text_format',
      '#title' => $this->t('Emergency Broadcast Message'),
      '#description' => t('Enter the message that will be displayed in the hompage as an emergencey broadcast.'),
      '#default_value' => \Drupal::state()->get('emergency_broadcast_message'),
    );

    $form['emergency_broadcast']['emergency_broadcast_active'] = array(
      '#type' => 'checkbox',
      '#title' => $this->t('Active?'),
      '#description' => t('The message will only be displayed if this is set to \'Active\'.'),
      '#default_value' => \Drupal::state()->get('emergency_broadcast_active'),
    );

    $form['recreation_map'] = array(
      '#type' => 'fieldset',
      '#title' => t('Recreation Map API'),
      '#description' => $this->t('Recreation Map settings will be added here. Please remove this line once this is so.'),
      '#collapsed' => FALSE,
      '#collapsible' => FALSE,
    );

    $form['site_information'] = array(
      '#type' => 'fieldset',
      '#title' => $this->t('Footer Information'),
      '#collapsed' => FALSE,
      '#collapsible' => FALSE,
    );

    $form['site_information']['postal_address'] = array(
      '#type' => 'text_format',
      '#title' => $this->t('Postal Address'),
      '#default_value' => \Drupal::state()->get('postal_address'),
      '#required' => TRUE,
    );

    $form['site_information']['contact_numbers'] = array(
      '#type' => 'text_format',
      '#title' => $this->t('Contact Numbers'),
      '#default_value' => \Drupal::state()->get('contact_numbers'),
      '#required' => TRUE,
    );

    $form['buttons']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Submit'),
    );

    return $form;
  }

  /**
   * Form validation handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    return $form;
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Save Water Level API settings.
    \Drupal::state()->set('water_levels_url', $form_state->getValues()['water_levels_url']);

    // Save Emergency Broadcast settings.
    \Drupal::state()->set('emergency_broadcast_message', $form_state->getValues()['emergency_broadcast_message']['value']);
    \Drupal::state()->set('emergency_broadcast_active', $form_state->getValues()['emergency_broadcast_active']);

    // Save Footer Information settings.
    \Drupal::state()->set('postal_address', $form_state->getValues()['postal_address']['value']);
    \Drupal::state()->set('contact_numbers', $form_state->getValues()['contact_numbers']['value']);
  }

}
